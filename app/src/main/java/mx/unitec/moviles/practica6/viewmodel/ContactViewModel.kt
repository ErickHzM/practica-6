package mx.unitec.moviles.practica6.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.unitec.moviles.practica6.model.Contact
import mx.unitec.moviles.practica6.repository.ContactsRepository

class ContactViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = ContactsRepository(application)

    val contacts = repository.getContacts()

    fun saveContact(contact :Contact) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(contact)
    }
}