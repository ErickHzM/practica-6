package mx.unitec.moviles.practica6.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import mx.unitec.moviles.practica6.model.Contact

@Dao
interface ContactDao {
    @Insert
    suspend fun insert(contact: Contact)

    @Query("SELECT * FROM " + Contact.TABLE_NAME + " ORDER BY name ")
    fun getOrderedAgenda() : LiveData<List<Contact>>
}